<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Ecommerce</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a1bad7ce-d1ff-4489-bfe7-bbc3ad5f195e</testSuiteGuid>
   <testCaseLink>
      <guid>f5ed6f95-dc14-49ea-bcd5-ad2ca7d1173e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Edit Profile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d652a8f-0ba6-44a0-8a6a-dd70beeb72d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Add cart - Home</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fe8769c-81fe-473e-9f26-1be14fa80897</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Add Cart - Recent</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
